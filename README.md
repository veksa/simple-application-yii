Simple applicaton on Yii2
============================

Simple application include CRUD, migration DB, and various connections to the database

**First deploy:**

1. git clone
2. composer install
3. php ./init
4. *fill config/bd.php file*
5. php ./yii migrate/up

**next deploy:**

1. git pull
2. composer update
3. php ./yii migrate/up