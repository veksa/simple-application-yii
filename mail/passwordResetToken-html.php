<?
use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl( [
	'user/default/reset-password',
	'token' => $user->password_reset_token
] );
?>
<div class="password-reset">
	<p>Доброе время суток, <?= Html::encode( $user->username ) ?>,</p>

	<p>Для восстановления пароля, перейдите по ссылке ниже:</p>

	<p><?= Html::a( Html::encode( $resetLink ), $resetLink ) ?></p>
</div>
