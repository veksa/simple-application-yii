<?

$resetLink = Yii::$app->urlManager->createAbsoluteUrl( [
	'/user/default/reset-password',
	'token' => $user->password_reset_token
] );
?>
Доброе время суток, <?= $user->username ?>,

Для восстановления пароля, перейдите по ссылке ниже:

<?= $resetLink ?>
