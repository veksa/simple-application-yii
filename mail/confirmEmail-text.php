<?
use yii\helpers\Html;

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl( [
	'user/default/confirm-email',
	'token' => $user->email_confirm_token
] );
?>

Доброе время суток, <?= Html::encode( $user->username ) ?>!

Для подтверждения адреса пройдите по ссылке:

<?= Html::a( Html::encode( $confirmLink ), $confirmLink ) ?>

Если Вы не регистрировались на нашем сайте, то просто удалите это письмо.