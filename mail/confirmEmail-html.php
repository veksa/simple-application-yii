<?
use yii\helpers\Html;

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl( [
	'user/default/confirm-email',
	'token' => $user->email_confirm_token
] );
?>
<div class="email-confirm">
	<p>Доброе время суток, <?= Html::encode( $user->username ) ?>!</p>

	<p>Для подтверждения адреса пройдите по ссылке:</p>

	<p><?= Html::a( Html::encode( $confirmLink ), $confirmLink ) ?></p>

	<p>Если Вы не регистрировались на нашем сайте, то просто удалите это письмо.</p>
</div>