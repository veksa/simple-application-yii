<?
use yii\db\Schema;
use yii\db\Migration;

class m150625_075316_createProjectModule extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%type}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->insert( '{{%type}}', [ 'name' => 'Стандартный' ] );
		$this->insert( '{{%type}}', [ 'name' => 'Сложный' ] );

		$this->createTable( '{{%project}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'type_id' => Schema::TYPE_INTEGER,
			'creator_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createTable( '{{%word}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%project_word}}', [
			'id' => Schema::TYPE_PK,
			'project_id' => Schema::TYPE_INTEGER,
			'word_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createIndex( 'FK_project_type', '{{%project}}', 'type_id' );
		$this->addForeignKey( 'FK_project_type', '{{%project}}', 'type_id', '{{%type}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'FK_project_creator', '{{%project}}', 'creator_id' );
		$this->addForeignKey( 'FK_project_creator', '{{%project}}', 'creator_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'FK_project_word_project', '{{%project_word}}', 'project_id' );
		$this->addForeignKey( 'FK_project_word_project', '{{%project_word}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE' );

		$this->createIndex( 'FK_project_word_word', '{{%project_word}}', 'word_id' );
		$this->addForeignKey( 'FK_project_word_word', '{{%project_word}}', 'word_id', '{{%word}}', 'id', 'CASCADE', 'CASCADE' );
	}

	public function down(){
		$this->dropForeignKey( 'FK_project_type', '{{%project}}' );
		$this->dropForeignKey( 'FK_project_creator', '{{%project}}' );
		$this->dropForeignKey( 'FK_project_word_project', '{{%project_word}}' );
		$this->dropForeignKey( 'FK_project_word_word', '{{%project_word}}' );

		$this->dropTable( '{{%project}}' );
		$this->dropTable( '{{%type}}' );
		$this->dropTable( '{{%word}}' );
		$this->dropTable( '{{%project_word}}' );
	}
}
