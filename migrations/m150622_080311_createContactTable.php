<?
use yii\db\Schema;
use yii\db\Migration;

class m150622_080311_createContactTable extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%contact}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'email' => Schema::TYPE_STRING . ' NOT NULL',
			'body' => Schema::TYPE_TEXT . ' NOT NULL'
		], $tableOptions );
	}

	public function down(){
		$this->dropTable( '{{%contact}}' );
	}
}
