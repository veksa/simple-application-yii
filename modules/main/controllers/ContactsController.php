<?
namespace app\modules\main\controllers;

use Yii;
use yii\web\Controller;
use app\modules\main\models\ContactForm;

class ContactsController extends Controller{
	public function actions(){
		return [
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : NULL,
			]
		];
	}

	public function actionIndex(){
		$model = new ContactForm();
		if( $model->load( Yii::$app->request->post() ) && $model->validate() ){
			if( $model->sendEmail( Yii::$app->params['adminEmail'] ) ){
				Yii::$app->session->setFlash( 'success', 'Спасибо, мы свяжемся с вами как можно быстрее.' );
			}else{
				Yii::$app->session->setFlash( 'error', 'Произошла ошибка на сервере, попробуйте позже.' );
			}

			return $this->refresh();
		}else{
			return $this->render( 'index', [
				'model' => $model
			] );
		}
	}
}

;