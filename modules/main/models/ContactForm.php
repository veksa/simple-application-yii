<?

namespace app\modules\main\models;

use Yii;
use yii\base\Model;

class ContactForm extends Model{
	public $name;
	public $email;
	public $body;
	public $verifyCode;

	public function attributeLabels(){
		return [
			'name' => 'Ваше имя',
			'email' => 'Ваш e-mail',
			'body' => 'Ваше сообщение',
			'verifyCode' => 'Код верификации'
		];
	}

	public function rules(){
		return [
			[
				[
					'name',
					'email',
					'body'
				],
				'required',
				'message' => 'Обязательное поле'
			],
			[
				'email',
				'email',
				'message' => 'Неверный формат'
			],
			[
				'verifyCode',
				'captcha',
				'captchaAction' => '/main/contacts/captcha',
				'message' => 'Неверный код верификации'
			]
		];
	}

	public function sendEmail( $email ){
		$model = new Contact;
		$model->name = $this->name;
		$model->email = $this->email;
		$model->body = $this->body;

		if( $model->save() ){
			return Yii::$app->mailer->compose()
				->setTo( $email )
				->setFrom( [ $this->email => $this->name ] )
				->setSubject( 'Сообщение с сайта Yii example' )
				->setTextBody( $this->body )
				->send();
		}

		return false;
	}
}
