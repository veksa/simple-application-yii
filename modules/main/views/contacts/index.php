<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>
		Для связи с нами используйте форму ниже.
	</p>

	<div class="row">
		<div class="col-lg-5">
			<? $form = ActiveForm::begin( [ 'id' => 'contact-form' ] ); ?>
			<?= $form->field( $model, 'name' ) ?>
			<?= $form->field( $model, 'email' ) ?>
			<?= $form->field( $model, 'body' )
				->textArea( [ 'rows' => 6 ] ) ?>
			<?= $form->field( $model, 'verifyCode' )
				->widget( Captcha::className(), [
					'captchaAction' => '/main/contacts/captcha',
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-9">{input}</div></div>',
				] ) ?>
			<div class="form-group">
				<?= Html::submitButton( 'Отправить', [
					'class' => 'btn btn-primary',
					'name' => 'contact-button'
				] ) ?>
			</div>
			<? ActiveForm::end(); ?>
		</div>
	</div>
</div>
