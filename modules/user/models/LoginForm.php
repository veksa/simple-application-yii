<?
namespace app\modules\user\models;

use Yii;
use yii\base\Model;

class LoginForm extends Model{
	public $username;
	public $password;
	public $rememberMe = true;

	private $_user = false;

	public function attributeLabels(){
		return [
			'username' => 'Ваш логин',
			'password' => 'Ваш пароль',
			'rememberMe' => 'Запомнить меня'
		];
	}

	public function rules(){
		return [
			[
				[
					'username',
					'password'
				],
				'required',
				'message' => 'Обязательное поле'
			],
			[
				'rememberMe',
				'boolean'
			],
			[
				'password',
				'validatePassword',
				'message' => 'Неверный логин или пароль'
			]
		];
	}

	public function validatePassword(){
		if( !$this->hasErrors() ){
			$user = $this->getUser();
			if( !$user || !$user->validatePassword( $this->password ) ){
				$this->addError( 'username', 'Неверный логин или пароль.' );
			}elseif( $user && $user->status == User::STATUS_BLOCKED ){
				$this->addError( 'username', 'Ваш аккаунт заблокирован.' );
			}elseif( $user && $user->status == User::STATUS_WAIT ){
				$this->addError( 'username', 'Ваш аккаунт не подтвержден.' );
			}
		}
	}

	public function getUser(){
		if( $this->_user === false ){
			$this->_user = User::findByUsername( $this->username );
		}

		return $this->_user;
	}

	public function login(){
		if( $this->validate() ){
			return Yii::$app->user->login( $this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0 );
		}else{
			return false;
		}
	}
}
