<?
namespace app\modules\user\models;

use Yii;
use yii\base\Model;

class ResetPasswordForm extends Model{
	public $password;
	private $_user;

	public function __construct( $token, $config = [ ] ){
		if( empty( $token ) || !is_string( $token ) ){
			$this->addError( 'notoken', 'Отсутствует код подтверждения.' );
		}

		$this->_user = User::findByPasswordResetToken( $token );
		if( !$this->_user ){
			$this->addError( 'incorrecttoken', 'Неверный токен.' );
		}

		if( !$this->errors ){
			parent::__construct( $config );
		}
	}

	public function attributeLabels(){
		return [
			'password' => 'Пароль'
		];
	}

	public function rules(){
		return [
			[
				'password',
				'required',
				'message' => 'Обязательное поле'
			],
			[
				'password',
				'string',
				'min' => 6
			]
		];
	}

	public function resetPassword(){
		$user = $this->_user;
		$user->setPassword( $this->password );
		$user->removePasswordResetToken();

		return $user->save( false );
	}
}
