<?
namespace app\modules\user\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\project\models\Project;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface{
	const STATUS_BLOCKED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_WAIT = 2;

	public static function tableName(){
		return 'user';
	}

	public function rules(){
		return [
			[
				[
					'username',
					'email'
				],
				'required',
				'message' => 'Обязательное поле'
			],
			[
				[
					'username',
					'email'
				],
				'filter',
				'filter' => 'trim'
			],
			[
				'username',
				'unique',
				'targetClass' => '\app\modules\user\models\User',
				'message' => 'Пользователь с таким логином уже существует.'
			],
			[
				'username',
				'string',
				'min' => 2,
				'max' => 255
			],
			[
				'email',
				'email',
				'message' => 'Неверный формат'
			],
			[
				'email',
				'unique',
				'targetClass' => '\app\modules\user\models\User',
				'message' => 'E-mail занят другим пользователем.'
			],
			[
				'password',
				'string',
				'min' => 6
			],
			[
				'status',
				'in',
				'range' => array_keys( self::getStatusesArray() )
			]
		];
	}

	public static function getStatusesArray(){
		return [
			self::STATUS_BLOCKED => 'Заблокирован',
			self::STATUS_ACTIVE => 'Активен',
			self::STATUS_WAIT => 'Ожидает подтверждения'
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'username' => 'Логин',
			'password_hash' => 'Хеш пароля',
			'password_reset_token' => 'Токен восстановления пароля',
			'email' => 'E-mail',
			'auth_key' => 'Ключ авторизации',
			'email_confirm_token' => 'Токен подтверждения регистрации',
			'status' => 'Статус учетной записи',
			'created_at' => 'Дата создания',
			'updated_at' => 'Дата обновления'
		];
	}

	public function behaviors(){
		return [
			TimestampBehavior::className(),
		];
	}

	public function beforeSave( $insert ){
		if( parent::beforeSave( $insert ) ){
			if( $insert ){
				$this->generateAuthKey();
			}

			return true;
		}

		return false;
	}

	public function generateAuthKey(){
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	public static function findIdentity( $id ){
		return static::findOne( [
			'id' => $id,
			'status' => self::STATUS_ACTIVE
		] );
	}

	public static function findIdentityByAccessToken( $token, $type = NULL ){
		throw new NotSupportedException( '"findIdentityByAccessToken" is not implemented.' );
	}

	public function getId(){
		return $this->getPrimaryKey();
	}

	public static function findByUsername( $username ){
		return static::findOne( [
			'username' => $username
		] );
	}

	public static function findByPasswordResetToken( $token ){
		if( !static::isPasswordResetTokenValid( $token ) ){
			return NULL;
		}

		return static::findOne( [
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE
		] );
	}

	public static function isPasswordResetTokenValid( $token ){
		if( empty( $token ) ){
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode( '_', $token );
		$timestamp = (int)end( $parts );

		return $timestamp + $expire >= time();
	}

	public static function findByEmailConfirmToken( $email_confirm_token ){
		return static::findOne( [
			'email_confirm_token' => $email_confirm_token,
			'status' => self::STATUS_WAIT
		] );
	}

	public function getProjects(){
		return $this->hasMany( Project::className(), [ 'creator_id' => 'id' ] );
	}

	public function getStatusName(){
		$statuses = self::getStatusesArray();

		return isset( $statuses[$this->status] ) ? $statuses[$this->status] : '';
	}

	public function validatePassword( $password ){
		return Yii::$app->security->validatePassword( $password, $this->password_hash );
	}

	public function setPassword( $password ){
		$this->password_hash = Yii::$app->security->generatePasswordHash( $password );
	}

	public function generatePasswordResetToken(){
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	public function getAuthKey(){
		return $this->auth_key;
	}

	public function removePasswordResetToken(){
		$this->password_reset_token = NULL;
	}

	public function generateEmailConfirmToken(){
		$this->email_confirm_token = Yii::$app->security->generateRandomString();
	}

	public function removeEmailConfirmToken(){
		$this->email_confirm_token = NULL;
	}

	public function validateAuthKey( $authKey ){
		return $this->getAuthKey() === $authKey;
	}


}

;
