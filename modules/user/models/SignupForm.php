<?
namespace app\modules\user\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model{
	public $username;
	public $email;
	public $password;
	public $verifyCode;

	public function attributeLabels(){
		return [
			'username' => 'Ваш логин',
			'email' => 'Ваш e-mail',
			'password' => 'Ваш пароль',
			'verifyCode' => 'Код верификации'
		];
	}

	public function rules(){
		return [
			[
				[
					'username',
					'email',
					'password'
				],
				'required',
				'message' => 'Обязательное поле'
			],
			[
				[
					'username',
					'email'
				],
				'filter',
				'filter' => 'trim'
			],
			[
				'username',
				'unique',
				'targetClass' => '\app\modules\user\models\User',
				'message' => 'Пользователь с таким логином уже существует.'
			],
			[
				'username',
				'string',
				'min' => 2,
				'max' => 255
			],
			[
				'email',
				'email',
				'message' => 'Неверный формат'
			],
			[
				'email',
				'unique',
				'targetClass' => '\app\modules\user\models\User',
				'message' => 'E-mail занят другим пользователем.'
			],
			[
				'password',
				'string',
				'min' => 6
			],
			[
				'verifyCode',
				'captcha',
				'captchaAction' => '/user/default/captcha',
				'message' => 'Неверный код верификации'
			]
		];
	}

	public function signup(){
		if( $this->validate() ){
			$user = new User();
			$user->username = $this->username;
			$user->email = $this->email;
			$user->setPassword( $this->password );
			$user->status = User::STATUS_WAIT;
			$user->generateEmailConfirmToken();
			if( $user->save() ){
				Yii::$app->mailer->compose( [
					'html' => 'confirmEmail-html',
					'text' => 'confirmEmail-text'
				], [ 'user' => $user ] )
					->setFrom( [ Yii::$app->params['noreplyEmail'] => Yii::$app->name ] )
					->setTo( $this->email )
					->setSubject( 'Подтверждение регистрации ' . Yii::$app->name )
					->send();

				return $user;
			}
		}

		return NULL;
	}
}
