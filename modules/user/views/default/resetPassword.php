<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>Введите новый пароль:</p>

	<div class="row">
		<div class="col-lg-5">
			<? $form = ActiveForm::begin( [ 'id' => 'reset-password-form' ] ); ?>
			<?= $form->field( $model, 'password' )
				->passwordInput() ?>
			<div class="form-group">
				<?= Html::submitButton( 'Сохранить', [ 'class' => 'btn btn-primary' ] ) ?>
			</div>
			<? ActiveForm::end(); ?>
		</div>
	</div>
</div>
