<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>Для регистрации заполните форму ниже:</p>

	<div class="row">
		<div class="col-lg-5">
			<? $form = ActiveForm::begin( [ 'id' => 'form-signup' ] ); ?>
			<?= $form->field( $model, 'username' ) ?>
			<?= $form->field( $model, 'email' ) ?>
			<?= $form->field( $model, 'password' )
				->passwordInput() ?>
			<?= $form->field( $model, 'verifyCode' )
				->widget( Captcha::className(), [
					'captchaAction' => '/user/default/captcha',
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-9">{input}</div></div>',
				] ) ?>
			<div class="form-group">
				<?= Html::submitButton( 'Зарегистрироваться', [
					'class' => 'btn btn-primary',
					'name' => 'signup-button'
				] ) ?>
			</div>
			<? ActiveForm::end(); ?>
		</div>
	</div>
</div>
