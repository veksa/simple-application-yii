<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>Заполните e-mail, и мы вышлем вам ссылку на восстановление пароля.</p>

	<div class="row">
		<div class="col-lg-5">
			<? $form = ActiveForm::begin( [ 'id' => 'request-password-reset-form' ] ); ?>
			<?= $form->field( $model, 'email' ) ?>
			<div class="form-group">
				<?= Html::submitButton( 'Выслать', [ 'class' => 'btn btn-primary' ] ) ?>
			</div>
			<? ActiveForm::end(); ?>
		</div>
	</div>
</div>
