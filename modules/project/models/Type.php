<?
namespace app\modules\project\models;

use Yii;

/**
 * This is the model class for table "type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Project[] $project
 */
class Type extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'type';
	}

	public function rules(){
		return [
			[
				[ 'name' ],
				'required'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
		];
	}

	public function getProjects(){
		return $this->hasMany( Project::className(), [ 'type_id' => 'id' ] );
	}
}
