<?
namespace app\modules\project\models;

use Yii;

/**
 * This is the model class for table "word".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ProjectWord[] $projectWords
 */
class Word extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'word';
	}

	public function rules(){
		return [
			[
				[ 'name' ],
				'required'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Name',
		];
	}
}