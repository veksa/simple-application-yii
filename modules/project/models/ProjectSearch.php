<?
namespace app\modules\project\models;

use Yii;
use yii\data\ActiveDataProvider;

class ProjectSearch extends ProjectList{
	public function search( $params ){
		$query = ProjectList::find()
			->where( [
				'creator_id' => Yii::$app->user->getId()
			] );

		$query->joinWith( [
			'type',
			'creator',
			'words'
		] );

		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
			'sort' => [
				'attributes' => [
					'name'
				]
			],
			'pagination' => [
				'pageSize' => 10
			]
		] );

		$dataProvider->sort->attributes['type_name'] = [
			'asc' => [ 'type.name' => SORT_ASC ],
			'desc' => [ 'type.name' => SORT_DESC ]
		];

		$dataProvider->sort->attributes['creator_username'] = [
			'asc' => [ 'user.username' => SORT_ASC ],
			'desc' => [ 'user.username' => SORT_DESC ]
		];

		if( !( $this->load( $params ) && $this->validate() ) ){
			return $dataProvider;
		}

		$query->andFilterWhere( [
			'like',
			'project.name',
			$this->name
		] )
			->andFilterWhere( [
				'like',
				'type.name',
				$this->type_name
			] )
			->andFilterWhere( [
				'like',
				'user.username',
				$this->creator_username
			] )
			->andFilterWhere( [
				'like',
				'word.name',
				$this->word_name
			] );

		return $dataProvider;
	}
}