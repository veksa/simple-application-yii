<?
namespace app\modules\project\models;

use Yii;
use app\modules\user\models\User;
use voskobovich\behaviors\ManyToManyBehavior;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type_id
 * @property integer $creator_id
 *
 * @property User $creator
 * @property Type $type
 */
class Project extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'project';
	}

	public function rules(){
		return [
			[
				[
					'name'
				],
				'required',
				'message' => 'Обязательное поле'
			],
			[
				[
					'type_id',
					'creator_id'
				],
				'integer'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
			'type_id' => 'Тип',
			'creator_id' => 'Создатель',
			'words' => 'Ключевые слова'
		];
	}

	public function behaviors(){
		return [
			[
				'class' => ManyToManyBehavior::className(),
				'relations' => [
					'words' => 'words'
				]
			]
		];
	}

	public function getType(){
		return $this->hasOne( Type::className(), [ 'id' => 'type_id' ] );
	}

	public function getCreator(){
		return $this->hasOne( User::className(), [ 'id' => 'creator_id' ] );
	}

	public function getWords(){
		return $this->hasMany( Word::className(), [ 'id' => 'word_id' ] )
			->viaTable( 'project_word', [
				'project_id' => 'id'
			] );
	}

	public function beforeSave( $insert ){
		if( $this->isNewRecord ){
			$this->creator_id = Yii::$app->user->getId();
		}

		if( $_POST['Project']['words']['name'] ){
			$words = [ ];
			foreach( $_POST['Project']['words']['name'] as $word ){
				$model = Word::findOne( [ 'name' => $word ] );
				if( !$model ){
					$model = new Word;
					$model->name = $word;
					$model->save();

				}

				$words[] = $model->getPrimaryKey();
			}

			$this->words = $words;
		}

		return parent::beforeSave( $insert );
	}
}
