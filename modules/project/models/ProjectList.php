<?
namespace app\modules\project\models;

use Yii;
use app\modules\user\models\User;
use voskobovich\behaviors\ManyToManyBehavior;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type_id
 * @property integer $creator_id
 *
 * @property User $creator
 * @property Type $type
 */
class ProjectList extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'project';
	}

	public function rules(){
		return [
			[
				[
					'name',
					'type_name',
					'creator_username',
					'word_name'
				],
				'safe'
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
			'type_name' => 'Тип',
			'creator_username' => 'Создатель',
			'word_name' => 'Ключевые слова'
		];
	}

	public function attributes(){
		return array_merge( parent::attributes(), [
			'type_name',
			'creator_username',
			'word_name'
		] );
	}

	public function getAttributes( $names = NULL, $except = [ ] ){
		$attributes = parent::getAttributes( $names = NULL, $except = [ ] );

		return array_replace( $attributes, [
			'words' => $this->words
		] );
	}

	public function behaviors(){
		return [
			[
				'class' => ManyToManyBehavior::className(),
				'relations' => [
					'words' => 'words'
				]
			]
		];
	}

	public function getType(){
		return $this->hasOne( Type::className(), [ 'id' => 'type_id' ] );
	}

	public function getCreator(){
		return $this->hasOne( User::className(), [ 'id' => 'creator_id' ] );
	}

	public function getWords(){
		return $this->hasMany( Word::className(), [ 'id' => 'word_id' ] )
			->viaTable( 'project_word', [
				'project_id' => 'id'
			] );
	}
}
