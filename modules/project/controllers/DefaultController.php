<?
namespace app\modules\project\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\modules\project\models\Type;
use app\modules\project\models\Project;
use app\modules\project\models\ProjectSearch;

class DefaultController extends Controller{
	public function actionIndex(){
		$searchModel = new ProjectSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->get() );

		return $this->render( 'index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel
		] );
	}

	public function actionView( $id ){
		$model = Project::findOne( $id );
		if( $model === NULL ){
			throw new NotFoundHttpException;
		}

		return $this->render( 'view', [
			'model' => $model
		] );
	}

	public function actionCreate(){
		$model = new Project;

		if( $model->load( Yii::$app->request->post() ) && $model->save() ){
			return $this->redirect( [
				'view',
				'id' => $model->id
			] );
		}else{
			$model->type_id = Type::find()
				->orderBy( [ 'id' => SORT_ASC ] )
				->one()->id;

			return $this->render( 'create', [
				'model' => $model
			] );
		}
	}

	public function actionUpdate( $id ){
		$model = Project::findOne( [ 'id' => $id ] );

		if( $model->load( Yii::$app->request->post() ) && $model->save() ){
			return $this->redirect( [
				'view',
				'id' => $model->id
			] );
		}else{
			return $this->render( 'update', [
				'model' => $model
			] );
		}
	}

	public function actionDelete( $id ){
		Project::findOne( [ 'id' => $id ] )
			->delete();

		return $this->redirect( [
			'index'
		] );
	}
}
