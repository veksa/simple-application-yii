<?
use yii\helpers\Html;

$this->title = 'Проект "' . $model->name . '"';
$this->params['breadcrumbs'][] = [
	'label' => 'Список проектов',
	'url' => [ '/project/default/index' ]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="projects-view">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<div class="type">
		Тип проекта: <?= $model->type->name ?>
	</div>
	<div class="creator">
		Создатель: <?= $model->creator->username ?>
	</div>
	<div class="words">
		Ключевые слова:
		<?
		$words = [ ];

		if( $model->words ){
			foreach( $model->words as $word ){
				$words[] = $word->name;
			}
		}

		echo implode( ', ', $words );
		?>
	</div>
</div>