<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\project\models\Type;
use unclead\widgets\MultipleInput;
?>

<? $form = ActiveForm::begin(); ?>
<?= $form->field( $model, 'name' ) ?>
<?= $form->field( $model, 'type_id' )
	->radioList( ArrayHelper::map( Type::find()
		->all(), 'id', 'name' ) ) ?>
<?= $form->field( $model, 'words' )
	->widget( MultipleInput::className(), [
		'limit' => 5,
		'columns' => [
			[
				'name' => 'name',
				'type' => 'textInput'
			]
		]
	] ) ?>
<?= Html::submitButton( Yii::t( 'app', $model->isNewRecord ? 'Создать' : 'Сохранить' ), [ 'class' => 'btn btn-primary' ] ) ?>
<? ActiveForm::end(); ?>