<?
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Список проектов';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="projects-list">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= Html::a( 'Добавить проект', [ '/project/default/create' ], [ 'class' => 'btn btn-primary' ] ) ?>

	<?= GridView::widget( [
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			[ 'class' => 'yii\grid\SerialColumn' ],
			'name',
			[
				'attribute' => 'type_name',
				'value' => function ( $model, $index, $dataColumn ){
					return $model->type->name;
				}
			],
			[
				'attribute' => 'creator_username',
				'value' => function ( $model, $index, $dataColumn ){
					return $model->creator->username;
				}
			],
			[
				'attribute' => 'word_name',
				'format' => 'raw',
				'value' => function ( $model ){
					$words = [ ];
					if( $model->words ){
						foreach( $model->words as $word ){
							$words[] = $word->name;
						}
					}

					return implode( ', ', $words );
				}
			],
			[
				'class' => 'yii\grid\ActionColumn'
			]
		]
	] ) ?>
</div>
