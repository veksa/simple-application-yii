<?
use yii\helpers\Html;

$this->title = 'Создание проекта';
$this->params['breadcrumbs'][] = [
	'label' => 'Список проектов',
	'url' => [ '/project/default/index' ]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="projects-view">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<?= $this->render( '_form', [ 'model' => $model ] ) ?>
</div>