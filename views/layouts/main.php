<?
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\widgets\Alert;

AppAsset::register( $this );
?>
<? $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode( $this->title ) ?></title>
	<? $this->head() ?>
</head>
<body>

<? $this->beginBody() ?>
<div class="wrap">
	<?
	NavBar::begin( [
		'brandLabel' => \Yii::$app->name,
		'brandUrl' => \Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	] );

	$menuItems = [
		[
			'label' => 'Главная',
			'url' => [ '/main/default/index' ]
		],
		[
			'label' => 'Контакты',
			'url' => [ '/main/contacts/index' ]
		]
	];

	if( Yii::$app->user->isGuest ){
		$menuItems[] = [
			'label' => 'Регистрация',
			'url' => [ '/user/default/signup' ]
		];
		$menuItems[] = [
			'label' => 'Авторизация',
			'url' => [ '/user/default/login' ]
		];
	}else{
		$menuItems[] = [
			'label' => 'Проекты',
			'url' => [ '/project/default/index' ]
		];
		$menuItems[] = [
			'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
			'url' => [ '/user/default/logout' ],
			'linkOptions' => [ 'data-method' => 'post' ]
		];
	}

	echo Nav::widget( [
		'options' => [ 'class' => 'navbar-nav navbar-right' ],
		'items' => $menuItems
	] );
	NavBar::end();
	?>

	<div class="container">
		<?= Breadcrumbs::widget( [
			'links' => isset( $this->params['breadcrumbs'] ) ? $this->params['breadcrumbs'] : [ ],
			'homeLink' => [
				'label' => 'Главная',
				'url' => Yii::$app->homeUrl
			]
		] ) ?>
		<?= Alert::widget() ?>
		<?= $content ?>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<p class="pull-left">&copy; <?= \Yii::$app->name ?> <?= date( 'Y' ) ?></p>

		<p class="pull-right"><?= Yii::powered() ?></p>
	</div>
</footer>

<? $this->endBody() ?>
</body>
</html>
<? $this->endPage() ?>
